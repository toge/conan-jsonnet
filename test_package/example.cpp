#include <iostream>

extern "C" {
#include "libjsonnet.h"
}

int main() {
   std::cout << jsonnet_version() << std::endl;
}
