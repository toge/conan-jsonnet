from conans import ConanFile, CMake, tools
import os

class JsonnetConan(ConanFile):
    name = "jsonnet"
    version = "0.17.0"
    license = "Apache-2.0"
    author = "toge.mail@gmail.com"
    url = "https://bitbucket.org/toge/conan-jsonnet/"
    homepae = "https://github.com/google/jsonnet/"
    description = "Jsonnet - The data templating language"
    topics = ("jsonnet", "json", "configuration")
    settings = "os", "compiler", "build_type", "arch"
    options = {"shared": [True, False]}
    default_options = {"shared": False}
    generators = "cmake"

    def source(self):
        tools.get("https://github.com/google/jsonnet/archive/v{}.zip".format(self.version))
        os.rename("jsonnet-{}".format(self.version), "jsonnet")

    def build(self):
        cmake = CMake(self)
        # cmake.verbose = True
        cmake.definitions["USE_SYSTEM_JSON"] = False
        cmake.definitions["BUILD_JSONNET"] = False
        cmake.definitions["BUILD_JSONNETFMT"] = False
        cmake.definitions["BUILD_TESTS"] = False
        cmake.definitions["BUILD_STATIC_LIBS"] = not self.options.shared
        cmake.definitions["BUILD_SHARED_BINARIES"] = self.options.shared
        cmake.configure(source_folder="jsonnet")
        if self.options.shared:
            cmake.build(target="libjsonnet")
            cmake.build(target="libjsonnet++")
        else:
            cmake.build(target="libjsonnet_static")
            cmake.build(target="libjsonnet++_static")

    def package(self):
        self.copy("*.h", dst="include", src="jsonnet/include")
        self.copy("*.lib", dst="lib", keep_path=False)
        self.copy("*.dll", dst="bin", keep_path=False)
        self.copy("*.so", dst="lib", keep_path=False)
        self.copy("*.dylib", dst="lib", keep_path=False)
        self.copy("*.a", dst="lib", keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["jsonnet++", "jsonnet", "md5", ]
